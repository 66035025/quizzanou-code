<?php

namespace App\Entity;

use App\Repository\ReponseJoueurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReponseJoueurRepository::class)
 */
class ReponseJoueur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Joueur::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $Joueur;

    /**
     * @ORM\ManyToOne(targetEntity=SessionQuizz::class, inversedBy="tempsReponse")
     * @ORM\JoinColumn(nullable=false)
     */
    private $SessionQuizz;

    /**
     * @ORM\Column(type="time")
     */
    private $tempsReponse;

    /**
     * @ORM\ManyToOne(targetEntity=ChoixReponse::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $ChoixReponse;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->Joueur;
    }

    public function setJoueur(?Joueur $Joueur): self
    {
        $this->Joueur = $Joueur;

        return $this;
    }

    public function getSessionQuizz(): ?SessionQuizz
    {
        return $this->SessionQuizz;
    }

    public function setSessionQuizz(?SessionQuizz $SessionQuizz): self
    {
        $this->SessionQuizz = $SessionQuizz;

        return $this;
    }

    public function getTempsReponse(): ?\DateTimeInterface
    {
        return $this->tempsReponse;
    }

    public function setTempsReponse(\DateTimeInterface $tempsReponse): self
    {
        $this->tempsReponse = $tempsReponse;

        return $this;
    }

    public function getChoixReponse(): ?ChoixReponse
    {
        return $this->ChoixReponse;
    }

    public function setChoixReponse(?ChoixReponse $ChoixReponse): self
    {
        $this->ChoixReponse = $ChoixReponse;

        return $this;
    }
}
