<?php

namespace App\Entity;

use App\Repository\SessionQuizzRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SessionQuizzRepository::class)
 */
class SessionQuizz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Quizz::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $Quizz;

    /**
     * @ORM\Column(type="datetime")
     */
    private $temps_debut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $tempsFin;

    /**
     * @ORM\Column(type="smallint")
     */
    private $codeAcces;

    /**
     * @ORM\OneToMany(targetEntity=ReponseJoueur::class, mappedBy="SessionQuizz")
     */
    private $tempsReponse;

    public function __construct()
    {
        $this->tempsReponse = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizz(): ?Quizz
    {
        return $this->Quizz;
    }

    public function setQuizz(?Quizz $Quizz): self
    {
        $this->Quizz = $Quizz;

        return $this;
    }

    public function getTempsDebut(): ?\DateTimeInterface
    {
        return $this->temps_debut;
    }

    public function setTempsDebut(\DateTimeInterface $temps_debut): self
    {
        $this->temps_debut = $temps_debut;

        return $this;
    }

    public function getTempsFin(): ?\DateTimeInterface
    {
        return $this->tempsFin;
    }

    public function setTempsFin(\DateTimeInterface $tempsFin): self
    {
        $this->tempsFin = $tempsFin;

        return $this;
    }

    public function getCodeAcces(): ?int
    {
        return $this->codeAcces;
    }

    public function setCodeAcces(int $codeAcces): self
    {
        $this->codeAcces = $codeAcces;

        return $this;
    }

    /**
     * @return Collection|ReponseJoueur[]
     */
    public function getTempsReponse(): Collection
    {
        return $this->tempsReponse;
    }

    public function addTempsReponse(ReponseJoueur $tempsReponse): self
    {
        if (!$this->tempsReponse->contains($tempsReponse)) {
            $this->tempsReponse[] = $tempsReponse;
            $tempsReponse->setSessionQuizz($this);
        }

        return $this;
    }

    public function removeTempsReponse(ReponseJoueur $tempsReponse): self
    {
        if ($this->tempsReponse->contains($tempsReponse)) {
            $this->tempsReponse->removeElement($tempsReponse);
            // set the owning side to null (unless already changed)
            if ($tempsReponse->getSessionQuizz() === $this) {
                $tempsReponse->setSessionQuizz(null);
            }
        }

        return $this;
    }
}
