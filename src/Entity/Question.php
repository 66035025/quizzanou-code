<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Quizz::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Quizz;

    /**
     * @ORM\Column(type="integer")
     */
    private $tempsImparti;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    public function __toString(){
        return $this->intitule;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuizz(): ?Quizz
    {
        return $this->Quizz;
    }

    public function setQuizz(?Quizz $Quizz): self
    {
        $this->Quizz = $Quizz;

        return $this;
    }
    public function getTempsImparti()
    {
        return $this->tempsImparti;
    }

    public function setTempsImparti($tempsImparti): self
    {
        $this->tempsImparti = $tempsImparti;

        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }
}
