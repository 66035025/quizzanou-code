<?php

namespace App\Form;

use App\Entity\ChoixReponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoixReponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('nbPoints', ChoiceType::class,[
                'choices'  => [
                    '25 points' => 25,
                    '50 points' => 50,
                    '75 points' => 75,
                    '100 points' => 100,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChoixReponse::class,
        ]);
    }
}
