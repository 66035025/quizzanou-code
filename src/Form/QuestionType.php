<?php

namespace App\Form;

use App\Entity\Question;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tempsImparti', ChoiceType::class,[
                'choices'  => [
                    '5 secondes' => 5,
                    '10 secondes' => 10,
                    '15 secondes' => 15,
                    '20 secondes' => 20,
                ],
            ])
            ->add('intitule')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
