<?php

namespace App\Repository;

use App\Entity\ReponseJoueur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReponseJoueur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReponseJoueur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReponseJoueur[]    findAll()
 * @method ReponseJoueur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseJoueurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReponseJoueur::class);
    }

    // /**
    //  * @return ReponseJoueur[] Returns an array of ReponseJoueur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReponseJoueur
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
