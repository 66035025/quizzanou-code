<?php

namespace App\Controller;

use App\Entity\ChoixReponse;
use App\Entity\Question;
use App\Form\ChoixReponseType;
use App\Repository\ChoixReponseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reponse")
 */
class ChoixReponseController extends AbstractController
{
    /**
     * @Route("/", name="choix_reponse_index", methods={"GET"})
     */
    public function index(ChoixReponseRepository $choixReponseRepository): Response
    {
        return $this->render('choix_reponse/index.html.twig', [
            'choix_reponses' => $choixReponseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{question}", name="choix_reponse_new", methods={"GET","POST"})
     */
    public function new(Request $request, Question $question): Response
    {
        $choixReponse = new ChoixReponse();
        $form = $this->createForm(ChoixReponseType::class, $choixReponse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($choixReponse);
            $entityManager->flush();

            return $this->redirectToRoute('choix_reponse_index');
        }

        return $this->render('choix_reponse/new.html.twig', [
            'choix_reponse' => $choixReponse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="choix_reponse_show", methods={"GET"})
     */
    public function show(ChoixReponse $choixReponse): Response
    {
        return $this->render('choix_reponse/show.html.twig', [
            'choix_reponse' => $choixReponse,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="choix_reponse_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ChoixReponse $choixReponse): Response
    {
        $form = $this->createForm(ChoixReponseType::class, $choixReponse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('choix_reponse_index');
        }

        return $this->render('choix_reponse/edit.html.twig', [
            'choix_reponse' => $choixReponse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="choix_reponse_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ChoixReponse $choixReponse): Response
    {
        if ($this->isCsrfTokenValid('delete'.$choixReponse->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($choixReponse);
            $entityManager->flush();
        }

        return $this->redirectToRoute('choix_reponse_index');
    }
}
