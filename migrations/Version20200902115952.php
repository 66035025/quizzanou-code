<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200902115952 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, mdp VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE choix_reponse (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, nb_points SMALLINT NOT NULL, INDEX IDX_796A26BE1E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE joueur (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, quizz_id INT NOT NULL, temps_imparti TIME NOT NULL, intitule VARCHAR(255) NOT NULL, INDEX IDX_B6F7494EBA934BCD (quizz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quizz (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, titre_quizz VARCHAR(255) NOT NULL, date_creation DATETIME NOT NULL, actif_desactive TINYINT(1) NOT NULL, INDEX IDX_7C77973DFB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reponse_joueur (id INT AUTO_INCREMENT NOT NULL, joueur_id INT NOT NULL, session_quizz_id INT NOT NULL, choix_reponse_id INT NOT NULL, temps_reponse TIME NOT NULL, INDEX IDX_2925705CA9E2D76C (joueur_id), INDEX IDX_2925705C313F2BA5 (session_quizz_id), INDEX IDX_2925705CC7381D31 (choix_reponse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_quizz (id INT AUTO_INCREMENT NOT NULL, quizz_id INT NOT NULL, temps_debut DATETIME NOT NULL, temps_fin DATETIME NOT NULL, code_acces SMALLINT NOT NULL, INDEX IDX_A70A2FEBBA934BCD (quizz_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, mdp VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE choix_reponse ADD CONSTRAINT FK_796A26BE1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494EBA934BCD FOREIGN KEY (quizz_id) REFERENCES quizz (id)');
        $this->addSql('ALTER TABLE quizz ADD CONSTRAINT FK_7C77973DFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE reponse_joueur ADD CONSTRAINT FK_2925705CA9E2D76C FOREIGN KEY (joueur_id) REFERENCES joueur (id)');
        $this->addSql('ALTER TABLE reponse_joueur ADD CONSTRAINT FK_2925705C313F2BA5 FOREIGN KEY (session_quizz_id) REFERENCES session_quizz (id)');
        $this->addSql('ALTER TABLE reponse_joueur ADD CONSTRAINT FK_2925705CC7381D31 FOREIGN KEY (choix_reponse_id) REFERENCES choix_reponse (id)');
        $this->addSql('ALTER TABLE session_quizz ADD CONSTRAINT FK_A70A2FEBBA934BCD FOREIGN KEY (quizz_id) REFERENCES quizz (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reponse_joueur DROP FOREIGN KEY FK_2925705CC7381D31');
        $this->addSql('ALTER TABLE reponse_joueur DROP FOREIGN KEY FK_2925705CA9E2D76C');
        $this->addSql('ALTER TABLE choix_reponse DROP FOREIGN KEY FK_796A26BE1E27F6BF');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494EBA934BCD');
        $this->addSql('ALTER TABLE session_quizz DROP FOREIGN KEY FK_A70A2FEBBA934BCD');
        $this->addSql('ALTER TABLE reponse_joueur DROP FOREIGN KEY FK_2925705C313F2BA5');
        $this->addSql('ALTER TABLE quizz DROP FOREIGN KEY FK_7C77973DFB88E14F');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE choix_reponse');
        $this->addSql('DROP TABLE joueur');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE quizz');
        $this->addSql('DROP TABLE reponse_joueur');
        $this->addSql('DROP TABLE session_quizz');
        $this->addSql('DROP TABLE utilisateur');
    }
}
